# The Portmod KSP Package repository

This repository contains packages for use with Kerbal Space Program.

Containing few packages itself, you will want to make use of portmod's [CKAN](https://gitlab.com/portmod/ckan) repository.
